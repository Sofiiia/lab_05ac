﻿#include <stdio.h>
#include <stdlib.h>
#include<windows.h>
#include <time.h>
#include <chrono>
#include <stdbool.h>
#define MAX_N 100000

typedef int elemtype;

struct elem {
	elemtype* value;
	struct elem* next;
	struct elem* prev;
};

struct myList {
	struct elem* head;
	struct elem* tail;
	size_t size;
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}
// Видаляємо список
void deleteList(cList* list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}

// Перевіряємо список на пустоту
bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}

// Додаємо новий вузол в кінець списку
int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}
	node->value = data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return(0);
}

// Далі видаляємо вузол з кінця списку
int popBack(cList* list, elemtype* data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return(-4);
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	data = node->value;
	list->size--;
	free(node);
	return(0);
}


// Виводимо список у консоль
void printList(cList* list, void (*func)(elemtype*)) {
	cNode* node = list->head;
	if (isEmptyList(list)) {
		return;
	}
	while (node) {
		func(node->value);
		node = node->next;
	}
}

cList* createList(void);
void deleteList(cList* list);
bool isEmptyList(cList* list);
int pushBack(cList* list, elemtype* data);
void printList(cList* list, void (*fun)(elemtype*));
void printNode(elemtype* value) {
	printf("%d ", *((int*)value));
}
void menu()
{
	system("cls");
	printf("1. Сортування вибором (двусвязний список)\n");
	printf("2. Сортування вставками (масив)\n");
	printf("3. Сортування вставками (двусвязний список)\n");
	printf("4. Вихід\n");
	printf(" Введіть пункт меню: ");
}
int get_variant(int count) {
	int variant;
	scanf_s("%d", &variant);
	return variant;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cList* mylist = createList();
	cList* mylist2 = createList();
	int list[MAX_N], arr[MAX_N], a, b, n, c, min;
	int variant, k;
	srand(time(0));
	printf("Введіть діапазон чисел: [a;b];\na = "); scanf_s("%d", &a);
	printf("b = "); scanf_s("%d", &b);


	do {
		menu();
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
		variant = get_variant(5);
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		switch (variant)
		{
		case 1:
		{
			printf("\n\n Відсортований зв'язний список(вибором) \n\n");
			int n[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };

			for (int k = 0; k < 7; k++) {
				cList* mylist = createList(); // Створюємо новий пустий список на кожній ітерації

				for (int i = 0; i < n[k]; i++) {
					list[i] = a + rand() % (b - a + 1);
					pushBack(mylist, &list[i]);
				}

				auto begin = std::chrono::steady_clock::now();
				cNode* current = mylist->head;

				while (current) {
					cNode* minNode = current;
					cNode* temp = current->next;

					while (temp) {
						if (*(int*)temp->value < *(int*)minNode->value) {
							minNode = temp;
						}
						temp = temp->next;
					}

					// Обмін значень між поточним елементом і мінімальним
					elemtype* tempValue = current->value;
					current->value = minNode->value;
					minNode->value = tempValue;

					current = current->next;
					
				}
				printList(mylist, printNode);
				auto end = std::chrono::steady_clock::now();
				auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
				printf("Час сортування списку на %d елементів: %lld ns\n", n[k], elapsed_ns.count());
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);

				deleteList(mylist); // Звільнення пам'яті, щоб уникнути витоку пам'яті
			}
		}

		break;
		case 2:
		{
			printf("\n\n Відсортований масив(вставки) \n\n");
			int n[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
			for (int k = 0;k < 7;k++) {

				for (int i = 0; i < n[k]; i++)
					arr[i] = a + rand() % (b - a + 1);
				//for (int i = 0; i < n[k]; i++)
					//printf("%4d", arr[i]);
				auto begin = std::chrono::steady_clock::now();
				for (int i = 1; i < n[k]; i++)
				{
					c = arr[i];// запомним обрабатываемый элемент
					// и начнем перемещение элементов слева от него
					// пока запомненный не окажется меньше чем перемещаемый
					for (int j = i - 1; j >= 0 && arr[j] > c; j--)
					{
						arr[j + 1] = arr[j];
						//Перебираємо масив та розміщуємо  елем. в правильному положенні в масиві
						arr[j] = c;
					}
				}
				
				for (int i = 0; i < n[k]; i++)
					printf("%4d", arr[i]);
				printf("\n");
				auto end = std::chrono::steady_clock::now();
				auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
				printf("\nЧас сортування списку на [%d]: %lld ms\n", n[k], elapsed_ns.count());
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
				printf("\n");
			}
		}
		break;
		case 3:
		{
			printf("\n\n Відсортований зв'язний список(вставками) \n\n");
			int n[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };

			for (int k = 0; k < 7; k++) {
				cList* mylist2 = createList(); // Створюємо новий порожній список на кожній ітерації циклу

				for (int i = 0; i < n[k]; i++) {
					list[i] = a + rand() % (b - a + 1);
					pushBack(mylist2, &list[i]);
				}

				auto begin = std::chrono::steady_clock::now();
				cNode* current = mylist2->head;

				while (current->next) {
					cNode* nextNode = current->next;
					elemtype* currentData = current->value;

					while (current->prev && *(int*)currentData < *(int*)current->prev->value) {
						current->value = current->prev->value;
						current = current->prev;
					}
					current->value = currentData;

					current = nextNode;
				}
				printList(mylist2, printNode);
				auto end = std::chrono::steady_clock::now();
				auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
				
				printf("Час сортування списку на %d елементів: %lld ns\n", n[k], elapsed_ns.count());
				

				deleteList(mylist2); // Звільнення пам'яті, щоб уникнути витоку пам'яті
			}
		}break;
		case 4:
			break;
		}
		if (variant != 10)
			system("pause");

	} while (variant != 4);
}
